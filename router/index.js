//路由拦截
// 必须要登录才能进入的页面
const needLogin = [ 
	
]
const list = ["navigateTo", "redirectTo", "reLaunch", "switchTab"];
list.forEach(item => { //用遍历的方式分别为,uni.navigateTo,uni.redirectTo,uni.reLaunch,uni.switchTab这4个路由方法添加拦截器
	console.log(item, 'router list item')
	uni.addInterceptor(item, {
		invoke(e) { // 调用前拦截
			// console.log('拦截模块', e);
			const token = uni.getStorageSync('token')
			if (needLogin.includes(e.url) && !token) {
				uni.showToast({
					title: '该页面需要登录才能访问，请先登录',
					icon: 'none'
				})
				uni.navigateTo({
					url: '/pageA/pages/login/index'
				})
				return false
			}
			return true
		},
		fail(err) { // 失败回调拦截 
			console.log(err);
		},
	})
})