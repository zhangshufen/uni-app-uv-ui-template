// 主包（首页几个）的请求接口
// 小程序模块一般不多。请求的接口比较少，如果模块很多，可以每个模块建一个js文件进行存储

// 轮播图
export const $banner = (data) => uni.$uv.http.get('banner', data)

// post请求，获取菜单
export const postMenu = (params, config = {}) => uni.$uv.http.post('/ebapi/public_api/index', params, config)

// get请求，获取菜单，注意：get请求的配置等，都在第二个参数中，详见前面解释
export const getMenu = (data) => uni.$uv.http.get('/ebapi/public_api/index', data)