import {
	load
} from './tools.js';
export const login = (code) => {
	load('登录中...')

	uni.login({
		uni.login({
			provider: 'weixin', //使用微信登录
			success: function(loginRes) {
				console.log(loginRes);

				that.$toast('授权登录成功')
				uni.navigateBack({
					delta: 1
				})
			},
			fail: function(error) {
				that.$toast('获取失败')
			}
		});
	})
}