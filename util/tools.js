// 封装的全局工具

// 提示框
export const toast = (title) => {
	uni.showToast({
		title: title,
		duration: 1000,
		mask: true,
	});
}

// loading框
export const load = (title) => {
	uni.showLoading({
		title: title,
		mask: true,
	});
}

// 预览图片
export const preImgs = ({
	url = '',
	index = 0
} = {}) => {
	uni.previewImage({
		urls: url, 
		current: index,
		longPressActions: {
			itemList: ['发送给朋友', '保存图片', '收藏'],
			success: function(data) {
				// console.log('选中了第' + (data.tapIndex + 1) + '个按钮,第' + (data.index + 1) + '张图片');
			},
			fail: function(err) {
				// console.log(err.errMsg);
				uni.showToast({
					title: '预览失败',
					duration::1000,
					mask: false,
				})
			}
		}
	});
}





 